// ****** Set up default POSTGRES connection START ****** //
require('dotenv').config();

const jwt = require('jsonwebtoken');
require('dotenv').config();
const User = require('../models/user').User;
const _ = require('lodash');
const { noRoleError } = require('../errors/error');

const authenticateUser_C = input => {
  //return User.find({ _id: input.id }, { roles: 1 }); // do not feed password back to query, password stays in database
  return User.findOne({
    where: { id: input.id }
  }).then((res) => {
    return [{
      id: res.dataValues.id,
      name: res.dataValues.name,
      email: res.dataValues.email,
      country: res.dataValues.country,
      state: res.dataValues.state,
      phone_code: res.dataValues.phone_code,
      phone: res.dataValues.phone,
      skills: res.dataValues.skills.split(','),
      dob: res.dataValues.dob,
      roles: res.dataValues.roles.split(',')
    }];
  }
  );
};

const getUsers_C = input => {
  //return User.find({ _id: input.id }, { roles: 1 }); // do not feed password back to query, password stays in database
  return User.findAll({
    offset: input.offset,
    limit: input.limit
  }).then((res) => {
    const resData = res.map(x => (
      {
        id: x.dataValues.id,
        name: x.dataValues.name,
        email: x.dataValues.email,
        country: x.dataValues.country,
        state: x.dataValues.state,
        phone_code: x.dataValues.phone_code,
        phone: x.dataValues.phone,
        skills: x.dataValues.skills.split(','),
        dob: x.dataValues.dob
      }
    ));
    return [{users: resData}];
  });
};

const checkUserExists_C = input => {
  //return User.find({ email: input.email }, { name:1, email: 1, roles: 1 });
  return User.findOne({
    where: { email: input.email }
  }).then((res) => {
    return [{
      name: res.dataValues.name,
      email: res.dataValues.email,
      roles: res.dataValues.roles.split(',')
    }];
  }
  );
};

const loginUser_C = input => {
  return User.findOne({
    where: { email: input.email, password: input.password }
  }).then((res) => {
    if(res) {
      return [{
        token: jwt.sign(
          { id: res.dataValues.id, email: res.dataValues.email, name: res.dataValues.name },
          process.env.JWT_SECRET,
          { expiresIn: '3d' }
        )
      }];
    }
  })
  .catch(err => {
    console.log(err);
  });
  // do not feed password back to query, password stays in database
}
const addUser_C = input => {
  input.roles = ["dummy"]; // assign a dummy roles at first time user is created
  let user = new User(input);
  return User.findOne({
    where: { email: input.email }
  }).then((res) => {
    if(res) {
      return {name:"",email:"", password: ""};
    } else {
      return User.create({ 
        name: input.name, 
        email: input.email, 
        password: input.password, 
        country: input.country,
        state: input.state,
        phone_code: input.phone_code,
        phone: input.phone,
        skills: input.skills.join(','),
        dob: input.dob,
        roles: input.roles.join(',') 
      }).then((res) => {
        return input;
      });
    }
  }).catch(err => {
    console.log(err);
  });
}

const updateUser_C = input => {
  // don't let user update his own role, only admin can update roles
  User.update({ 
    name: input.name, 
    email: input.email, 
    password: input.password, 
    country: input.country,
    state: input.state,
    phone_code: input.phone_code,
    phone: input.phone,
    skills: input.skills.join(','),
    dob: input.dob
  }, { where: { id: input.id }, returning: true }).then((res) => {
    return res[1][0].dataValues;
  })
  .catch(err => {
    console.log(err);
  });
};

const updateUserAdmin_C = input => {
  console.log("input:"+input)
  return User.findOne({
    where: { id: input.myid }
  }).then((res) => {
    if (_.intersectionWith(res.dataValues.roles.split(','), input.expectedRoles, _.isEqual).length >= 1) {
      // don't let user update his own role, only admin can update roles
      User.update({ roles: input.roles.join(',') }, { where: { id: input.id } }).then((res) => {
        return input;
      });
    } else {
      //throw new noRoleError();
    }
  }
  );
};

module.exports = {
  authenticateUser_C,
  getUsers_C,
  checkUserExists_C,
  loginUser_C,
  addUser_C,
  updateUser_C,
  updateUserAdmin_C
};