const express = require('express');
const graphqlHTTP = require('express-graphql');
const cors = require('cors');
const app = express();
// store config variables in dotenv
require('dotenv').config();

// postgres schema
const schema = require('./schema/Schema');

// ****** allow cross-origin requests code START ****** //
//app.use(cors()); // uncomment this to enable all CORS and delete cors(corsOptions) in below code
var allowedOrigins = process.env.allowedOrigins.split(',');
app.use(cors({
    origin: function (origin, callback) {
        // allow requests with no origin 
        // (like mobile apps or curl requests)
        if (!origin) return callback(null, true);

        if (allowedOrigins.indexOf(origin) === -1) {
            var msg = 'The CORS policy for this site does not ' + 'allow access from the specified Origin.';
            return callback(new Error(msg), false);
        }
        return callback(null, true);
    }
}));
// ****** allow cross-origin requests code END ****** //

// bind express with graphql
app.use('/graphql', graphqlHTTP({
    schema, // this is setup to use POSTGRES for now
    // graphiql: false
    graphiql: true
}));
app.use('/', (req, res) => res.send("Welcome AuthAssignment User"));
app.listen(process.env.GRAPHQLPORT, () => console.log('AuthAssignment Server is ready on localhost:' + process.env.GRAPHQLPORT));
