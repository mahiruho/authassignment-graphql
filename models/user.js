
// ****** Set up default POSTGRES connection START ****** //
require('dotenv').config();
const Sequelize = require('sequelize');
const sequelize = new Sequelize(process.env.DB_NAME, process.env.DB_USERNAME, process.env.DB_PASSWORD, {
  host: process.env.DB_HOST,
  dialect: process.env.DB_DIALECT,
  operatorsAliases: false,
  pool: { max: 5,min: 0, acquire: 30000, idle: 10000 }
});
const Schema = sequelize.Schema;

sequelize.authenticate().then(() => { console.log('connected to POSTGRES- AuthAssignment database');})
  .catch(err => {
    console.error('Unable to connect to the database:', err);
  });
// ****** Set up default MYSQL connection END ****** //

const User = sequelize.define('users', {
  name: { type: Sequelize.STRING },
  email: { type: Sequelize.STRING },
  password: { type: Sequelize.STRING },
  country: { type: Sequelize.INTEGER },
  state: { type: Sequelize.INTEGER },
  phone_code: { type: Sequelize.STRING },
  phone: { type: Sequelize.STRING },
  skills: { type: Sequelize.STRING },
  dob: { type: Sequelize.DATE },
  roles: { type: Sequelize.STRING }
});

// force: true will drop the table if it already exists
// User.sync({force: true}).then(() => {
//   // Table created
//   return User.create({
//     name: 'Ajit Kumar Thakur',
//     email: 'ajit.thakur101@gmail.com',
//     password: 'Ajit@101',
//     country: 101,
//     state: 4017,
//     phone_code: '91',
//     phone: '7278078482',
//     skills: 'Developer,BDE',
//     dob: '2019-12-03 00:00:00+05:30',
//     roles: "admin"
//   });
// });

module.exports = { User }